Workshop: AWS Lab (Base)
===

This repository serves as a universal base provisioning tool for workshops driven in AWS. Uses variable input for instance size, how many students supported, and desired region to build out a core labbing environment.

![Network Diagram](/images/network_diagram.png)

Requirements
===

* [AWS Account Access Key & ID](https://docs.aws.amazon.com/general/latest/gr/aws-sec-cred-types.html#access-keys-and-secret-access-keys)

    * Install `awscli`
    * Run `aws configure` and input your key/secret to configure your default profile
* [Terraform](https://learn.hashicorp.com/tutorials/terraform/install-cli)


Variables
===
* Copy `terraform.tfvars.example` to `terraform.tfvars`
* Fill out your `terraform.tfvars` file
* Run `terraform init` to download dependencies
* Run `terraform apply` to apply the plan to your cloud tenancy

| Variable | Description | Options | Default Value |
|--|--|--|--|
| `input_region` | Desired target region | us-east-1, us-east-2, us-west-1, us-west-2, af-south-1, ap-east-1, ap-south-1, ap-northeast-3, ap-northeast-2, ap-southeast-1, ap-southeast-2, ap-northeast-1, ca-central-1, eu-central-1, eu-west-1, eu-west-2, eu-south-1, eu-west-3, eu-north-1, me-south-1, sa-east-1  | us-east-1 | 
| `input_vpc_name` | VPC Name | Type: `string`. Try to keep it short. See `terraform.tfvars` for an example of defaults. | workshop-aws-lab |
| `input_ssh_path` | Path to where you want your keys to be generated | Example: `~/.ssh/` | ~/.ssh/ |
| `input_ssh_key_name` | Base name of the generated keys | Example: `workshop` | workshop |
| `input_instance_type` | AWS EC2 Instance Type | See [AWS Instance Types](https://aws.amazon.com/ec2/instance-types/) for more information | t2.small |
| `input_instance_count` | Number of desired supported students. Deploys one instance per count. | Number (1- X) | 5 |
<br>

Contributing
===
Please see [CONTRIBUTING.md](CONTRIBUTING.md)

Contact
===

* Chris Kuperstein <ckuperst@redhat.com>
* Josh Springer <jspringe@redhat.com>
* Lester Claudio <claudiol@redhat.com>