# Create a data object to pull the latest RHEL image (can update for other images)
data "aws_ami" "latest-rhel" {
most_recent = true
owners = ["309956199498"] # Red Hat
  filter {
      name   = "name"
      values = ["RHEL-8*"]
  }
  filter {
      name   = "architecture"
      values = ["x86_64"]
  }
  filter {
      name   = "root-device-type"
      values = ["ebs"]
  }
}
