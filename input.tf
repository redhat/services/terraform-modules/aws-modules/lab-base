variable "input_region" {
    type = string
    description = "The region which you want to deploy the lab components to."
    default = "us-east-1"
}

variable "input_vpc_name" {
    type = string
    description = "Name of the VPC"
    default = "workshop-aws-lab"
}


variable "input_instance_count" {
    type = number
    description = "Number of student working nodes."
    default = 5
}

variable "input_instance_type" {
    type = string
    description = "Amazon EC2 instance type for student working nodes."
    default = "t2.small"
}

variable "input_ssh_path" {
    type = string
    description = "Path to your SSH key"
    default = "~/.ssh/"
}

variable "input_ssh_key_name" {
    type = string
    description = "Base name of the SSH key created by Terraform."
    default = "workshop"
}
