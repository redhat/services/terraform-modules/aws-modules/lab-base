# Generating a random hex for creating unique names
resource "random_id" "environment" {
    byte_length = 8
}

# Generate a private SSH key
resource "tls_private_key" "generated_key" {
  algorithm = "RSA"
  rsa_bits  = 4096
  provisioner "local-exec" {
  # Creates a PEM file on your local host
    command = "echo '${self.private_key_pem}' > ${var.input_ssh_path}${var.input_ssh_key_name}-${random_id.environment.hex}.pem"
  }
  # Give the pem file a valid permission
  provisioner "local-exec" {
    command = "chmod 0600 ${var.input_ssh_path}${var.input_ssh_key_name}-${random_id.environment.hex}.pem"
  }
}

# Create the keypair in AWS
resource "aws_key_pair" "generated_key" {
  key_name   = "${var.input_ssh_key_name}-${random_id.environment.hex}"
  public_key = tls_private_key.generated_key.public_key_openssh
  tags = {
    Terraform = "true"
    Environment = random_id.environment.hex
  }
}

module "vpc" {
  source = "git@github.com:terraform-aws-modules/terraform-aws-vpc.git"
  name = var.input_vpc_name
  cidr = "10.10.0.0/16"
  azs             = ["${var.input_region}a", "${var.input_region}b", "${var.input_region}c"]
  private_subnets = ["10.10.1.0/24"]
  public_subnets  = ["10.10.101.0/24"]
  enable_nat_gateway = true
  enable_vpn_gateway = false # Disabling this until it becomes necessary
  tags = {
    Terraform = "true"
    Environment = random_id.environment.hex
  }
}

module "bastion_node_sg" {
  source = "git@github.com:terraform-aws-modules/terraform-aws-security-group.git"
  name        = "bastion"
  description = "Security group for bastion with SSH ports open within VPC"
  vpc_id      = module.vpc.vpc_id
  ingress_cidr_blocks   = ["0.0.0.0/0"]
  ingress_rules         = ["ssh-tcp"]
  ingress_with_self     = [{ "rule" = "all-all" }]
  egress_rules          = ["all-all"]
  egress_with_self      = []
}

module "bastion_node" {
    source                  = "git@github.com:terraform-aws-modules/terraform-aws-ec2-instance.git"
    name                    = "workshop-bastion"
    instance_count          = 1
    ami              = "${data.aws_ami.latest-rhel.id}"
    instance_type    = "t2.micro"
    key_name         = aws_key_pair.generated_key.key_name
    monitoring       = false
    associate_public_ip_address = true
    vpc_security_group_ids      = [module.vpc.default_security_group_id, module.bastion_node_sg.security_group_id]
    subnet_id                   = module.vpc.public_subnets[0]
    root_block_device = [
        {
        volume_type = "gp2"
        volume_size = 35
        }
    ]

    tags                    = {
        Terraform   = true
        Environment = random_id.environment.hex
    }
}

module "workshop_instances" {
    source                  = "git@github.com:terraform-aws-modules/terraform-aws-ec2-instance.git"
    name                    = "workshop-student"
    instance_count          = var.input_instance_count
    ami              = "${data.aws_ami.latest-rhel.id}"
    instance_type    = var.input_instance_type
    key_name         = aws_key_pair.generated_key.key_name
    monitoring       = false
    vpc_security_group_ids      = [module.vpc.default_security_group_id]
    subnet_id                   = module.vpc.private_subnets[0]
    root_block_device = [
        {
        volume_type = "gp2"
        volume_size = 35
        }
    ]
    tags                    = {
        Terraform   = true
        Environment = random_id.environment.hex
    }
}