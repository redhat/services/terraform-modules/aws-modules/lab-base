output "aws_workshop_id"{
  value = random_id.environment.hex
}

output "aws_key_pair_name" {
  value = aws_key_pair.generated_key.key_name
}

output "ssh_private_key" {
  value = tls_private_key.generated_key.private_key_pem
  sensitive = true
}

output "ssh_public_key" {
  value = aws_key_pair.generated_key.public_key
}

output "workshop_bastion_public_ip"{
  description = "This is the public IP of your bastion node."
  value = module.bastion_node.public_ip
}

output "workshop_nodes_ip"{
  description = "This is the list of private IPs from your workshop hosts"
  value = module.workshop_instances.private_ip
}

output "aws_public_subnet_id" {
  value = module.vpc.public_subnets[0]
}